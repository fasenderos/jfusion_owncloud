<?php
/**
 * file containing helper functions for ownCloud
 * 
 * PHP version 5
 * 
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage OwnCloud
 * @author     Andrea Fassina <fiscalway@fiscalway.it>
 * @copyright  2014 Fiscalway. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.fiscalway.it
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * JFusion Hooks for dokuwiki
 * 
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage OwnCloud
 * @author     Andrea Fassina <fiscalway@fiscalway.it>
 * @copyright  2014 Fiscalway. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.fiscalway.it
 */

class JFusionHelper_owncloud {

    /**
     * @return string
     */
    function getJname()
    {
        return 'owncloud';
    }
    /**
     * @param $dir
     */
	 function delete_directory($dir) {
        $handle = opendir($dir);
        if ($handle) {
            while (false !== ($file = readdir($handle))){
                if ($file != '.' && $file != '..'){
                    if(is_dir($dir.$file)){
                        if(!@rmdir($dir.$file)){ // Empty directory? Remove it
                            $this->delete_directory($dir.$file.'/'); // Not empty? Delete the files inside it
                        }
                    } else {
                        @unlink($dir.$file);
                    }
                }
            }
            closedir($handle);
            @rmdir($dir);
        }
    }
}