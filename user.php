<?php

/**
 * JFusion User Class for OwnCloud
 * 
 * PHP version 5
 * 
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage OwnCloud
 * @author     Andrea Fassina <fiscalway@fiscalway.it>
 * @copyright  2014 Fiscalway. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.fiscalway.it
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * load the jplugin model
 */
require_once(JPATH_ADMINISTRATOR .DS.'components'.DS.'com_jfusion'.DS.'models'.DS.'model.jfusion.php');
require_once(JPATH_ADMINISTRATOR .DS.'components'.DS.'com_jfusion'.DS.'models'.DS.'model.abstractuser.php');
require_once(JPATH_ADMINISTRATOR .DS.'components'.DS.'com_jfusion'.DS.'models'.DS.'model.jplugin.php');


/**
 * JFusion User Class for OwnCloud
 * For detailed descriptions on these functions please check the model.abstractuser.php
 * 
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage OwnCloud
 * @author     Andrea Fassina <fiscalway@fiscalway.it>
 * @copyright  2014 Fiscalway. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.fiscalway.it
 */
class JFusionUser_owncloud extends JFusionUser {

    /**
     * @param object $userinfo
     *
     * @return null|object
     */
    function getUser($userinfo) {
        //get the identifier
        $identifier = $userinfo;
        if (is_object($userinfo)) {
            $identifier = $userinfo->username;
        }
        // Get user info from database
        $db = JFusionFactory::getDatabase($this->getJname());
		$query = 'SELECT c.id as userid,a.uid as username, a.displayname as name, a.password, b.configvalue as email FROM #__users as a JOIN #__preferences as b JOIN #__clndr_calendars as c ON a.uid = b.userid WHERE b.userid = ' . $db->Quote($identifier) . ' AND b.configkey = "email" AND c.userid = ' . $db->Quote($identifier);

        $db->setQuery($query);
        $result = $db->loadObject();
        
        return $result;
    }

    /**
     * returns the name of this JFusion plugin
     * @return string name of current JFusion plugin
     */    
    function getJname() 
    {
        return 'owncloud';
    }
	
    /**
     * @param object $userinfo
     *
     * @return array
     */
function deleteUser($userinfo) {
		//setup status array to hold debug info and errors
		$status = array('error' => array(),'debug' => array());
		$params = JFusionFactory::getParams($this->getJname());
		$db = JFusionFactory::getDatabase($this->getJname());
		
		$query = 'DELETE FROM #__users WHERE uid = '.$db->quote($userinfo->username);
		$db->setQuery($query);
        if (!$db->query()) {
       		$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
        } else {
			$query = 'DELETE From #__group_user WHERE uid = '.$db->quote($userinfo->username);
	  		$db->setQuery($query);
			if (!$db->query()) {
				$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
			} else {
				$query = 'DELETE From #__preferences WHERE userid = '.$db->quote($userinfo->username);
	  			$db->setQuery($query);
       		 	if (!$db->query()) {
				$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
        		} else {
					$query = 'DELETE FROM #__contacts_cards WHERE addressbookid IN (SELECT id FROM #__contacts_addressbooks WHERE userid = '.$db->quote($userinfo->username).')';
					$db->setQuery($query);
					if (!$db->query()) {
					$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
					} else {
						$query = 'DELETE FROM #__contacts_cards_properties WHERE userid = '.$db->quote($userinfo->username);
						$db->setQuery($query);
						if (!$db->query()) {
						$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
						} else {
							$query = 'DELETE From #__contacts_addressbooks WHERE userid = '.$db->quote($userinfo->username);
							$db->setQuery($query);
							if (!$db->query()) {
							$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
							} else {
								$query = 'DELETE FROM #__documents_op WHERE es_id IN (SELECT es_id FROM #__documents_member WHERE uid = '.$db->quote($userinfo->username).')';
								$db->setQuery($query);
								if (!$db->query()) {
								$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
								} else {
									$query = 'DELETE From #__documents_member WHERE uid = '.$db->quote($userinfo->username);
									$db->setQuery($query);
									if (!$db->query()) {
									$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
									} else {
										$query = 'DELETE From #__documents_session WHERE owner = '.$db->quote($userinfo->username);
										$db->setQuery($query);
										if (!$db->query()) {
										$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
										} else {
											$query = 'DELETE From #__vcategory WHERE uid = '.$db->quote($userinfo->username);
											$db->setQuery($query);
											if (!$db->query()) {
											$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
											} else {
												$query = 'DELETE FROM #__clndr_objects WHERE calendarid IN (SELECT id FROM #__clndr_calendars WHERE userid = '.$db->quote($userinfo->username).')';
												$db->setQuery($query);
												if (!$db->query()) {
												$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
												} else {
													$query = 'DELETE From #__clndr_calendars WHERE userid = '.$db->quote($userinfo->username);
													$db->setQuery($query);
													if (!$db->query()) {
													$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
													} else {
														$query = $db->getQuery(true)
														->delete('#__storages')
														->where('id = ' . $db->quote('home::' . $userinfo->username));
														$db->setQuery($query);
														$db->execute(); 
														if (!$db->query()) {
														$status['error'][] = JText::_('USER_CREATION_ERROR') . $db->stderr();
														} else {
															$query = $db->getQuery(true)
															->delete('#__filecache')
															->where('path = ' . $db->quote('%' . $userinfo->username . '%'));
															$db->setQuery($query);
															$db->execute(); 
															if (!$db->query()) {
															$status['error'][] = JText::_('USER_CREATION_ERROR') . $db->stderr();
																} else {
																$query = 'DELETE From #__activity WHERE user = '.$db->quote($userinfo->username);
																$db->setQuery($query);
																if (!$db->query()) {
																$status['error'][] = JText::_('USER_DELETION_ERROR') . ' ' .  $db->stderr();
																} else {
																	$uploadpath = $params->get('upload_path');
																	$user_dir = $uploadpath . DS . $userinfo->username . DS;
																	if (is_dir($user_dir)) {
																	$helper = JFusionFactory::getHelper($this->getJname());
																	$helper->delete_directory($user_dir); //Remove user folder including subfolders and files
																	} 
																	
																	//return the good news
																	$status['debug'][] = JText::_('USER_DELETION');
																	$status['userinfo'] = $this->getUser($userinfo);
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return $status;
    }

    /**
     * @param object $userinfo
     * @param array $option
     *
     * @return array
     */
    function destroySession($userinfo, $option) {
		$status = array('error' => array(),'debug' => array());
	    $params = JFusionFactory::getParams($this->getJname());

	    $status = JFusionJplugin::destroySession($userinfo, $options, $this->getJname(), $params->get('logout_type'));

	    return $status;
    }

    /**
     * @param object $userinfo
     * @param array $options
     * @param bool $framework
     *
     * @return array
     */
    function createSession($userinfo, $options) {		
        $status = array('error' => array(),'debug' => array());

		$params = JFusionFactory::getParams($this->getJname());
        $status = JFusionJplugin::createSession($userinfo, $options,$this->getJname(),$params->get('brute_force'));
        
		return $status;
    }

    /**
     * @param string $username
     *
     * @return string
     */
    function filterUsername($username) {
        return $username;
    }
       
    function PasswordHash($iteration_count_log2, $portable_hashes)
	{
		$this->itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

		if ($iteration_count_log2 < 4 || $iteration_count_log2 > 31)
			$iteration_count_log2 = 8;
		$this->iteration_count_log2 = $iteration_count_log2;

		$this->portable_hashes = $portable_hashes;

		$this->random_state = microtime();
		if (function_exists('getmypid'))
			$this->random_state .= getmypid();
	}
	
	function HashPassword($password)
	{
		$random = '';

		if (CRYPT_BLOWFISH == 1 && !$this->portable_hashes) {
			$random = $this->get_random_bytes(16);
			$hash =
			    crypt($password, $this->gensalt_blowfish($random));
			if (strlen($hash) == 60)
				return $hash;
		}

		if (CRYPT_EXT_DES == 1 && !$this->portable_hashes) {
			if (strlen($random) < 3)
				$random = $this->get_random_bytes(3);
			$hash =
			    crypt($password, $this->gensalt_extended($random));
			if (strlen($hash) == 20)
				return $hash;
		}

		if (strlen($random) < 6)
			$random = $this->get_random_bytes(6);
		$hash =
		    $this->crypt_private($password,
		    $this->gensalt_private($random));
		if (strlen($hash) == 34)
			return $hash;

		# Returning '*' on error is safe here, but would _not_ be safe
		# in a crypt(3)-like function used _both_ for generating new
		# hashes and for validating passwords against existing hashes.
		return '*';
	}
	
	private static $hasher = null;
    
    private function getHasher() {
		if (!self::$hasher) {
			//we don't want to use DES based crypt(), since it doesn't return a hash with a recognisable prefix
			$forcePortable = (CRYPT_BLOWFISH != 1);
			self::$hasher = new PasswordHash(8, $forcePortable);
		}
		return self::$hasher;
	}
 	
    function updatePassword($userinfo, &$existinguser, &$status) {
        $params = JFusionFactory::getParams($this->getJname());
		$source_path = $params->get('source_path');
		if (substr($source_path, -1) == DS) {
            require $source_path . 'config' . DS . 'config.php';
        } else {
            require $source_path . DS. 'config' . DS . 'config.php';
        }		
        $password_salt = $CONFIG['passwordsalt'];
		$hasher = $this->getHasher();
		
		$existinguser->password = $hasher->HashPassword($userinfo->password_clear . $password_salt);
        $db = JFusionFactory::getDatabase($this->getJname());
        $query = 'UPDATE #__users SET password =' . $db->Quote($existinguser->password) . ' WHERE uid =' . $db->Quote($existinguser->username);
        $db->setQuery($query);
        if (!$db->query()) {
            $status['error'][] = JText::_('PASSWORD_UPDATE_ERROR') . $db->stderr();
        } else {
            $status['debug'][] = JText::_('PASSWORD_UPDATE') . ' ' . substr($existinguser->password, 0, 6) . '********';
        }
    }

    /**
     * @param object $userinfo
     * @param array &$status
     *
     * @return void
     */
   function createUser($userinfo, &$status)
    {
	//found out what usergroup should be used
		$db = JFusionFactory::getDatabase($this->getJname());
        $params = JFusionFactory::getParams($this->getJname());
        $usergroups = JFusionFunction::getCorrectUserGroups($this->getJname(),$userinfo);
        if (empty($usergroups)) {
            $status['error'][] = JText::_('ERROR_CREATE_USER') . ": " . JText::_('USERGROUP_MISSING');
        } else {
			$source_path = $params->get('source_path');
			if (substr($source_path, -1) == DS) {
				require $source_path . 'config' . DS . 'config.php';
				} else {
				require $source_path . DS. 'config' . DS . 'config.php';
			}		
			$password_salt = $CONFIG['passwordsalt'];
			$hasher = $this->getHasher();
            $usergroup = $usergroups[0];
            $username_clean = $this->filterUsername($userinfo->username);
            //prepare the variables
            $user = new stdClass;
			$user->uid = $userinfo->username;
            $user->displayname = $userinfo->name;
			$user->password = $hasher->HashPassword($userinfo->password_clear . $password_salt);
                       
            //now append the new user data
            if (!$db->insertObject('#__users', $user, 'uid' )) {
                //return the error
                $status['error'] = JText::_('USER_CREATION_ERROR'). ': ' . $db->stderr();
            } else {
                //update the stats
                $query = 'INSERT INTO #__clndr_calendars (id, userid, displayname, uri, active, ctag, calendarorder, calendarcolor, timezone, components) VALUES (NULL, ' . $db->quote($userinfo->username) . ', \'Personale\', \'personale\', 1, 1, 0, NULL, NULL, \'VEVENT,VTODO,VJOURNAL\')';
                $db->setQuery($query);
                if (!$db->query()) {
                    //return the error
                    $status['error'][] = JText::_('USER_CREATION_ERROR') . $db->stderr();
                } else {
                    $query = "INSERT INTO #__group_user (gid, uid) VALUES ( '$usergroup', '$userinfo->username')";
                    $db->setQuery($query);
                    if (!$db->query()) {
                        //return the error
                        $status['error'][] = JText::_('USER_CREATION_ERROR') . $db->stderr();
                    } else {
                        $query = 'INSERT INTO #__preferences (userid, appid, configkey, configvalue) VALUES (' . $db->quote($userinfo->username) . ', \'firstrunwizard\', \'show\', 0), (' . $db->quote($userinfo->username) . ', \'login\', \'lastLogin\', ' . time() . '), (' . $db->quote($userinfo->username) . ', \'settings\', \'email\', ' . $db->quote($userinfo->email) . ')';
                        $db->setQuery($query);
                        if (!$db->query()) {
                            //return the error
                            $status['error'][] = JText::_('USER_CREATION_ERROR') . $db->stderr();
                        } else {
							//return the good news
							$status['debug'][] = JText::_('USER_CREATION');
							$status['userinfo'] = $this->getUser($userinfo);
						}
                    }
                }
            }
        }
    }

    /**
     * @param object $userinfo
     * @param object $existinguser
     * @param array $status
     *
     * @return void
     */
    function updateEmail($userinfo, &$existinguser, &$status) {
        //we need to update the email
        $db = JFusionFactory::getDatabase($this->getJname());
        $query = 'UPDATE #__preferences SET configvalue ='.$db->quote($userinfo->email) .' WHERE configkey = "email" AND userid =' . $existinguser->userid;
        $db->setQuery($query);
        if (!$db->query()) {
            $status['error'][] = JText::_('EMAIL_UPDATE_ERROR') . $db->stderr();
        } else {
	        $status['debug'][] = JText::_('EMAIL_UPDATE'). ': ' . $existinguser->email . ' -> ' . $userinfo->email;
        }
    }
}
