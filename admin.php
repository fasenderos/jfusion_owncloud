<?php

/**
 * file containing administrator function for the jfusion plugin
 *
 * PHP version 5
 *
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage ownCloud
 * @author     Andrea Fassina <fiscalway@fiscalway.it>
 * @copyright  2014 Fiscalway. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.fiscalway.it
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * JFusion Admin Class for ownCloud
 * For detailed descriptions on these functions please check the model.abstractadmin.php
 *
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage ownCloud
 * @author     Andrea Fassina <fiscalway@fiscalway.it>
 * @copyright  2014 Fiscalway. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.fiscalway.it
 */
class JFusionAdmin_owncloud extends JFusionAdmin
{
    /**
     * returns the name of this JFusion plugin
     * @return string name of current JFusion plugin
     */
    function getJname()
    {
        return 'owncloud';
    }

    /**
     * @return string
     */
    function getTablename() {
        return 'users';
    }
	
	 /**
     * @param string $forumPath
     * @return array
     */
    function setupFromPath($forumPath)
    {
        //check for trailing slash and generate file path
        if (substr($forumPath, -1) == DS) {
            $myfile = $forumPath . 'config' . DS . 'config.php';
        } else {
            $myfile = $forumPath . DS. 'config' . DS . 'config.php';
        }
        //try to open the file
        $params = array();
		$configuration = array ();
		if (($file_handle = include($myfile)) === FALSE) {
            JError::raiseWarning(500,JText::_('WIZARD_FAILURE'). ": $myfile " . JText::_('WIZARD_MANUAL'));
        } else {
            //parse the given array to get the configration's values
            $file_handle = include($myfile);
            foreach ($CONFIG as $key => $value) {
				$configuration[$key] = $value;
			    }            
            //Save the parameters into the standard JFusion params format
            $params['database_host'] = $configuration['dbhost'];
            $params['database_name'] = $configuration['dbname'];
            $params['database_user'] = $configuration['dbuser'];
            $params['database_password'] = $configuration['dbpassword'];
            $params['database_prefix'] = $configuration['dbtableprefix'];
            $params['database_type'] = $configuration['dbtype'];
			$params['upload_path'] = $configuration['datadirectory'];
            $params['source_path'] = $forumPath;
        }
        //Save the parameters into the standard JFusion params format
        return $params;
    }

    /**
     * Get a list of users
     *
     * @param int $limitstart
     * @param int $limit
     *
     * @return array
     */
    function getUserList($limitstart = 0, $limit = 0) {
        //getting the connection to the db
        $db = JFusionFactory::getDatabase($this->getJname());
        $query = 'SELECT uid from #__users';
        $db->setQuery($query,$limitstart,$limit);
        //getting the results
        $userlist = $db->loadObjectList();
        return $userlist;
    }

    /**
     * @return int
     */
    function getUserCount() {
        //getting the connection to the db
        $db = JFusionFactory::getDatabase($this->getJname());
        $query = 'SELECT count(*) from #__users';
        $db->setQuery($query);
        //getting the results
        return $db->loadResult();
    }

    /**
     * @return array
     */
    function getUsergroupList() {
        //get the connection to the db
        $db = JFusionFactory::getDatabase($this->getJname());
        $query = 'SELECT gid as id, gid as name from #__groups;';
        $db->setQuery($query);
        //getting the results
        return $db->loadObjectList();
    }

    /**
     * @return string
     */
	function getDefaultUsergroup()
    {	
		$usergroups = JFusionFunction::getCorrectUserGroups($this->getJname(),null);
		$usergroup_id = null;
		if(!empty($usergroups)) {
			$usergroup_id = $usergroups[0];
		}

		$usergrouplist = $this->getUsergroupList();
		foreach ($usergrouplist as $value) {
			if($value ->id == $usergroup_id){
				return $value->name;
			}
		}
		return null;
    }
	
     /**
     * @return bool
     */
    function allowRegistration() {
        // Registration is not allowed. Only the admin can create user from the admin panel
        return false;
    }

    /**
     * do plugin support multi usergroups
     *
     * @return string UNKNOWN or JNO or JYES or ??
     */
    function requireFileAccess()
	{
		return 'JYES';
	}
	
	/**
     * do plugin support multi usergroups
     *
     * @return bool
     */
    function isMultiGroup()
    {
        return false;
    }

	/**
	 * @return bool do the plugin support multi instance
	 */
	function multiInstance()
	{
		return false;
	}
}
