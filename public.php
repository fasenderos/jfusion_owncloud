<?php

/**
 * JFusion Public Class for OwnCloud
 * 
 * PHP version 5
 * 
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage OwnCloud
 * @author     Andrea Fassina <fiscalway@fiscalway.it>
 * @copyright  2014 Fiscalway. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.fiscalway.it
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * JFusion Public Class for OwnCloud
 * For detailed descriptions on these functions please check the model.abstractpublic.php
 * 
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage OwnCloud
 * @author     Andrea Fassina <fiscalway@fiscalway.it>
 * @copyright  2014 Fiscalway. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.fiscalway.it
 */

class JFusionPublic_owncloud extends JFusionPublic {

    /**
     * returns the name of this JFusion plugin
     * @return string name of current JFusion plugin
     */    
    function getJname() 
    {
        return 'owncloud';
    }

    /**
     * @return string
     */
    function getRegistrationURL() {
        return '';
    }

    /**
     * @return string
     */
    function getLostPasswordURL() {
        return '';
    }

    /**
     * @return string
     */
    function getLostUsernameURL() {
        return '';
    }
}
