<?php

/**
 * JFusion Authentication Class for OwnCloud
 * 
 * PHP version 5
 * 
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage OwnCloud
 * @author     Andrea Fassina <fiscalway@fiscalway.it>
 * @copyright  2014 Fiscalway. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.fiscalway.it
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * JFusion Authentication Class for ownCloud
 * For detailed descriptions on these functions please check the model.abstractauth.php
 * 
 * @category   JFusion
 * @package    JFusionPlugins
 * @subpackage ownCloud 
 * @author     JFusion Team <webmaster@jfusion.org>
 * @copyright  2008 JFusion. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       http://www.jfusion.org
 */
class JFusionAuth_owncloud extends JFusionAuth {
	
	function PasswordHash($iteration_count_log2, $portable_hashes)
	{
		$this->itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

		if ($iteration_count_log2 < 4 || $iteration_count_log2 > 31)
			$iteration_count_log2 = 8;
		$this->iteration_count_log2 = $iteration_count_log2;

		$this->portable_hashes = $portable_hashes;

		$this->random_state = microtime();
		if (function_exists('getmypid'))
			$this->random_state .= getmypid();
	}
	
	function HashPassword($password)
	{
		$random = '';

		if (CRYPT_BLOWFISH == 1 && !$this->portable_hashes) {
			$random = $this->get_random_bytes(16);
			$hash =
			    crypt($password, $this->gensalt_blowfish($random));
			if (strlen($hash) == 60)
				return $hash;
		}

		if (CRYPT_EXT_DES == 1 && !$this->portable_hashes) {
			if (strlen($random) < 3)
				$random = $this->get_random_bytes(3);
			$hash =
			    crypt($password, $this->gensalt_extended($random));
			if (strlen($hash) == 20)
				return $hash;
		}

		if (strlen($random) < 6)
			$random = $this->get_random_bytes(6);
		$hash =
		    $this->crypt_private($password,
		    $this->gensalt_private($random));
		if (strlen($hash) == 34)
			return $hash;

		# Returning '*' on error is safe here, but would _not_ be safe
		# in a crypt(3)-like function used _both_ for generating new
		# hashes and for validating passwords against existing hashes.
		return '*';
	}
	
	private static $hasher = null;
    
    private function getHasher() {
		if (!self::$hasher) {
			//we don't want to use DES based crypt(), since it doesn't return a hash with a recognisable prefix
			$forcePortable = (CRYPT_BLOWFISH != 1);
			self::$hasher = new PasswordHash(8, $forcePortable);
		}
		return self::$hasher;
	}
	
	 function getJname()
    {
        return 'owncloud';
    }
 
     /**
     * @param array|object $userinfo
     * @return string
     */
    function generateEncryptedPassword($userinfo) {
		$params = JFusionFactory::getParams($this->getJname());
		$source_path = $params->get('source_path');
		if (substr($source_path, -1) == DS) {
            require $source_path . 'config' . DS . 'config.php';
        } else {
            require $source_path . DS. 'config' . DS . 'config.php';
        }		
        $password_salt = $CONFIG['passwordsalt'];
		$hasher = $this->getHasher();
		
		$testcrypt = $hasher->HashPassword($userinfo->password_clear . $password_salt);
			
        return $testcrypt;
    }
}
